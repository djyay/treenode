<?php

namespace App\Controller;

use App\Entity\Utilisateur;
use App\Repository\UtilisateurRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UtilisateurController extends AbstractController
{
    private $entityManager;
    private $utilisateurRepository;

    public function __construct(
        EntityManagerInterface $entityManager,
        UtilisateurRepository $utilisateurRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->utilisateurRepository = $utilisateurRepository;
    }

    /**
     * @Route("/utilisateur", name="utilisateur")
     */
    public function index(): Response
    {
        $utilisateur = new Utilisateur();
        $this->entityManager->persist($utilisateur);
        $this->entityManager->flush();

/*
        $utilisateur_new = (new Utilisateur());
        $utilisateur_new->setParent($utilisateur);

        $utilisateur3 = (new Utilisateur());
        $utilisateur3->setParent($utilisateur);


        $this->entityManager->persist($utilisateur);
        $this->entityManager->persist($utilisateur_new);
        $this->entityManager->persist($utilisateur3);
        $this->entityManager->flush();

        dump('enfant');
        dump($this->utilisateurRepository->getChildren($utilisateur,true));
*/

        return $this->render('utilisateur/index.html.twig', [
            'controller_name' => 'UtilisateurController',
        ]);
    }
}
